import pandas as pd
import matplotlib.pyplot as plt

df = pd.read_excel("BasepacticaDanielaLondono.xlsx", sheet_name="Hoja4")

print(df.head())

Valores = df[["Contratista", "ValorFinalContrato"]]

ax = Valores.plot.bar(y="ValorFinalContrato", rot=0)

plt.title('Valores finales de los contratos')
plt.ylabel('COP')
plt.xlabel('Contratistas')
plt.show()
